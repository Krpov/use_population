package com.example.usapopulation.core.presentation

import androidx.annotation.LayoutRes
import androidx.appcompat.app.AppCompatActivity
import com.example.usapopulation.R
import com.example.usapopulation.core.data.network.NetworkState
import com.github.terrakok.cicerone.NavigatorHolder
import com.github.terrakok.cicerone.androidx.AppNavigator
import dagger.hilt.android.scopes.ActivityScoped
import javax.inject.Inject

open class BaseActivity(@LayoutRes contentLayoutId: Int) :
    AppCompatActivity(contentLayoutId) {

    @ActivityScoped
    @Inject
    lateinit var networkState: NetworkState

    @ActivityScoped
    @Inject
    lateinit var navigatorHolder: NavigatorHolder

    override fun onStart() {
        super.onStart()
        networkState.registerNetworkStateObserver()
    }

    override fun onStop() {
        networkState.unregisterNetworkStateObserver()
        super.onStop()
    }

    override fun onResumeFragments() {
        super.onResumeFragments()
        navigatorHolder.setNavigator(
            AppNavigator(this, R.id.rcvContainer)
        )
    }

    override fun onPause() {
        navigatorHolder.removeNavigator()
        super.onPause()
    }
}
