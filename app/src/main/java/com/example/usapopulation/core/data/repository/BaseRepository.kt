package com.example.usapopulation.core.data.repository

import com.example.usapopulation.core.domain.model.DomainError
import com.google.gson.JsonIOException
import retrofit2.HttpException
import java.io.IOException

abstract class BaseRepository {

    fun <T> errorResult(throwable: Throwable?): Result<T> =
        when (throwable) {
            is IOException -> Result.failure(DomainError.ConnectionError)
            is HttpException -> {
                if (throwable.code() == HTTP_UNAUTHORIZED) {
                    Result.failure(DomainError.AuthorizationError)
                } else {
                    Result.failure(DomainError.ServiceUnavailableError)
                }
            }
            is JsonIOException -> Result.failure(DomainError.ServiceUnavailableError)
            else -> Result.failure(DomainError.ServiceUnavailableError)
        }

    companion object {

        private const val HTTP_UNAUTHORIZED = 401
    }
}
