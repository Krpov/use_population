package com.example.usapopulation.core.data.network

data class SessionState(
    var baseUrl: String,
    var authorized: Boolean
)
