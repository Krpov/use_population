package com.example.usapopulation.core.domain.model

sealed class DomainError : Throwable() {

    object ConnectionError : DomainError()

    object ServiceUnavailableError : DomainError()

    object AuthorizationError : DomainError()

    object EmptyDataError : DomainError()
}
