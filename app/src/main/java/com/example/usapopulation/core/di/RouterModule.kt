package com.example.usapopulation.core.di

import com.github.terrakok.cicerone.Cicerone
import com.github.terrakok.cicerone.NavigatorHolder
import com.github.terrakok.cicerone.Router
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@InstallIn(SingletonComponent::class)
@Module
object RouterModule {

    @Provides
    @Singleton
    fun provideCicerone(): Cicerone<Router> =
        Cicerone.create()

    @Provides
    @Singleton
    fun provideAppRouter(
        cicerone: Cicerone<Router>
    ): Router = cicerone.router

    @Provides
    @Singleton
    fun provideAppNavigatorHolder(
        cicerone: Cicerone<Router>
    ): NavigatorHolder = cicerone.getNavigatorHolder()
}
