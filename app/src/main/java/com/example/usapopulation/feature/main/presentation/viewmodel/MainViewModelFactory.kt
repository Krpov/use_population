package com.example.usapopulation.feature.main.presentation.viewmodel

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.example.usapopulation.feature.main.presentation.router.MainRouter

class MainViewModelFactory(
    private val mainRouter: MainRouter
) : ViewModelProvider.Factory {

    override fun <T : ViewModel> create(modelClass: Class<T>): T =
        MainViewModel(mainRouter) as T
}
