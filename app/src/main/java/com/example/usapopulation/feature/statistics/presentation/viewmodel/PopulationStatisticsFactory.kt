package com.example.usapopulation.feature.statistics.presentation.viewmodel

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.example.usapopulation.feature.statistics.domain.interactor.PopulationStatisticsInteractor
import com.example.usapopulation.feature.statistics.presentation.mapper.PopulationStatisticsErrorMapper

class PopulationStatisticsFactory(
    private val populationStatisticsInteractor: PopulationStatisticsInteractor,
    private val populationStatisticsErrorMapper: PopulationStatisticsErrorMapper
) : ViewModelProvider.Factory {
    override fun <T : ViewModel> create(modelClass: Class<T>): T =
        PopulationStatisticsViewModel(populationStatisticsInteractor, populationStatisticsErrorMapper) as T
}
