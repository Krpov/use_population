package com.example.usapopulation.feature.main.di

import android.content.Context
import com.example.usapopulation.core.data.network.NetworkState
import com.example.usapopulation.feature.main.presentation.router.MainRouter
import com.example.usapopulation.feature.main.presentation.viewmodel.MainViewModelFactory
import com.github.terrakok.cicerone.Router
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ActivityComponent
import dagger.hilt.android.qualifiers.ActivityContext
import dagger.hilt.android.scopes.ActivityScoped

@InstallIn(ActivityComponent::class)
@Module
object MainModule {

    @Provides
    @ActivityScoped
    fun provideNetworkState(
        @ActivityContext activity: Context
    ): NetworkState = NetworkState(activity)

    @Provides
    @ActivityScoped
    fun provideMainRouter(
        router: Router
    ): MainRouter = MainRouter(router)

    @Provides
    @ActivityScoped
    fun provideMainViewModelFactory(
        mainRouter: MainRouter
    ): MainViewModelFactory = MainViewModelFactory(mainRouter)
}
