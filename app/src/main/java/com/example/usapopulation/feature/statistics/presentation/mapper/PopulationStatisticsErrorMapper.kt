package com.example.usapopulation.feature.statistics.presentation.mapper

import com.example.usapopulation.R
import com.example.usapopulation.core.domain.mapper.IMapper
import com.example.usapopulation.core.domain.model.DomainError

class PopulationStatisticsErrorMapper: IMapper<Throwable, Int> {

    override fun map(from: Throwable): Int =
        when (from) {
            is DomainError.ConnectionError -> R.string.check_internet_connection_error
            is DomainError.EmptyDataError -> R.string.no_data_error
            else -> R.string.service_unavailable_error
        }
}
