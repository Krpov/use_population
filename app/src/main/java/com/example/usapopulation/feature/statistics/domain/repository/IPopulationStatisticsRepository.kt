package com.example.usapopulation.feature.statistics.domain.repository

import com.example.usapopulation.feature.statistics.domain.model.PopulationStatisticsEntity

interface IPopulationStatisticsRepository {

    suspend fun populationStatistics(): Result<PopulationStatisticsEntity>
}
