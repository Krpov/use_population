package com.example.usapopulation.feature.statistics.domain.model

data class PopulationStatisticEntity(
    val nation: String,
    val year: String,
    val population: String
)
