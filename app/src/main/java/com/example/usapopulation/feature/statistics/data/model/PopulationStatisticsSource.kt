package com.example.usapopulation.feature.statistics.data.model

import androidx.annotation.Keep

@Keep
data class PopulationStatisticsSource(
    val measures: List<String>?,
    val annotations: Annotations?,
    val name: String?,
    val substitutions: List<String>?
)
