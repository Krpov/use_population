package com.example.usapopulation.feature.statistics.di

import com.example.usapopulation.core.data.network.ApiClient
import com.example.usapopulation.core.data.network.NetworkState
import com.example.usapopulation.feature.statistics.data.api.PopulationStatisticsApi
import com.example.usapopulation.feature.statistics.data.mapper.PopulationStatisticsMapper
import com.example.usapopulation.feature.statistics.data.repository.PopulationStatisticsRepository
import com.example.usapopulation.feature.statistics.domain.interactor.PopulationStatisticsInteractor
import com.example.usapopulation.feature.statistics.domain.repository.IPopulationStatisticsRepository
import com.example.usapopulation.feature.statistics.presentation.mapper.PopulationStatisticsErrorMapper
import com.example.usapopulation.feature.statistics.presentation.viewmodel.PopulationStatisticsFactory
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.FragmentComponent
import dagger.hilt.android.scopes.ActivityScoped
import dagger.hilt.android.scopes.FragmentScoped

@InstallIn(FragmentComponent::class)
@Module
object PopulationStatisticsModule {

    @Provides
    @FragmentScoped
    fun providePopulationStatisticsApi(
        apiClient: ApiClient
    ): PopulationStatisticsApi = apiClient.createApiClient(PopulationStatisticsApi::class.java)

    @Provides
    @FragmentScoped
    fun providePopulationStatisticsMapper(): PopulationStatisticsMapper = PopulationStatisticsMapper()

    @Provides
    @FragmentScoped
    fun providePopulationStatisticsRepository(
        populationStatisticsApi: PopulationStatisticsApi,
        populationStatisticsMapper: PopulationStatisticsMapper,
        @ActivityScoped networkState: NetworkState
    ): IPopulationStatisticsRepository =
        PopulationStatisticsRepository(
            populationStatisticsApi, populationStatisticsMapper, networkState
        )

    @Provides
    @FragmentScoped
    fun providePopulationStatisticsInteractor(
        populationStatisticsRepository: IPopulationStatisticsRepository
    ): PopulationStatisticsInteractor =
        PopulationStatisticsInteractor(populationStatisticsRepository)

    @Provides
    @FragmentScoped
    fun providePopulationStatisticsErrorMapper(): PopulationStatisticsErrorMapper =
        PopulationStatisticsErrorMapper()

    @Provides
    @FragmentScoped
    fun providePopulationStatisticsFactory(
        populationStatisticsInteractor: PopulationStatisticsInteractor,
        populationStatisticsErrorMapper: PopulationStatisticsErrorMapper
    ): PopulationStatisticsFactory =
        PopulationStatisticsFactory(
            populationStatisticsInteractor,
            populationStatisticsErrorMapper
        )
}
