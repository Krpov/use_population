package com.example.usapopulation.feature.statistics.data.model

import androidx.annotation.Keep

@Keep
data class PopulationStatisticsResponse(
    val data: List<PopulationStatistics>?,
    val source: List<PopulationStatisticsSource>?
)
