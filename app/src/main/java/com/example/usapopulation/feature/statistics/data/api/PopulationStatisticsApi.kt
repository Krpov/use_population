package com.example.usapopulation.feature.statistics.data.api

import com.example.usapopulation.feature.statistics.data.model.PopulationStatisticsResponse
import retrofit2.http.GET

interface PopulationStatisticsApi {

    @GET("api/data?drilldowns=Nation&measures=Population")
    suspend fun populationStatistics(): PopulationStatisticsResponse
}
