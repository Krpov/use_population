package com.example.usapopulation.feature.statistics.domain.model

data class PopulationStatisticsEntity(
    val data: List<PopulationStatisticEntity>,
    val sourceDescription: String,
    var yearOrderDescending: Boolean = true
)
