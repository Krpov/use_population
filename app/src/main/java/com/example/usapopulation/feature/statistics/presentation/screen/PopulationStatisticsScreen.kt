package com.example.usapopulation.feature.statistics.presentation.screen

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentFactory
import com.github.terrakok.cicerone.androidx.FragmentScreen

class PopulationStatisticsScreen: FragmentScreen {

    override fun createFragment(factory: FragmentFactory): Fragment =
        PopulationStatisticsFragment()
}
