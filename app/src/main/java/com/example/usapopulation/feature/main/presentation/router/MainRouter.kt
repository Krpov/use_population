package com.example.usapopulation.feature.main.presentation.router

import com.example.usapopulation.feature.statistics.presentation.screen.PopulationStatisticsScreen
import com.github.terrakok.cicerone.Router

class MainRouter(
    private val router: Router
) {
    fun goToPopulationStatistics() {
        router.replaceScreen(PopulationStatisticsScreen())
    }
}
