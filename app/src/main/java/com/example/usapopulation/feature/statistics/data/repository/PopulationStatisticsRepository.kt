package com.example.usapopulation.feature.statistics.data.repository

import com.example.usapopulation.core.data.network.NetworkState
import com.example.usapopulation.core.data.repository.BaseRepository
import com.example.usapopulation.core.domain.model.DomainError
import com.example.usapopulation.feature.statistics.data.api.PopulationStatisticsApi
import com.example.usapopulation.feature.statistics.data.mapper.PopulationStatisticsMapper
import com.example.usapopulation.feature.statistics.domain.model.PopulationStatisticsEntity
import com.example.usapopulation.feature.statistics.domain.repository.IPopulationStatisticsRepository

class PopulationStatisticsRepository(
    private val populationStatisticsApi: PopulationStatisticsApi,
    private val populationStatisticsMapper: PopulationStatisticsMapper,
    private val networkState: NetworkState
) : BaseRepository(), IPopulationStatisticsRepository {

    override suspend fun populationStatistics(): Result<PopulationStatisticsEntity> =
        try {
            if (networkState.isNetworkAvailable) {
                Result.success(
                    populationStatisticsMapper.map(
                        populationStatisticsApi.populationStatistics()
                    )
                )
            } else {
                errorResult(DomainError.ConnectionError)
            }
        } catch (throwable: Throwable) {
            errorResult(throwable)
        }
}
