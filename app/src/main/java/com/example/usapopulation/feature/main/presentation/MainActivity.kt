package com.example.usapopulation.feature.main.presentation

import androidx.activity.viewModels
import com.example.usapopulation.R
import com.example.usapopulation.core.presentation.BaseActivity
import com.example.usapopulation.feature.main.presentation.viewmodel.MainViewModel
import com.example.usapopulation.feature.main.presentation.viewmodel.MainViewModelFactory
import dagger.hilt.android.AndroidEntryPoint
import dagger.hilt.android.scopes.ActivityScoped
import javax.inject.Inject

/*
Запрос https://datausa.io/api/data?drilldowns=Nation&measures=Population
// TODO Необходимо реализовать выше запрос (авторизация не требуется) и вывести значение на экран ввиде списка элементов и
// TODO сделать возможность сортировать список по годам в порядке возрастания/убывания
Для сокращения времени рутинной работы в package "model" создан валидный для ответ на запрос выше в классе "PopulationResponse".
Также в ApiClient лежит базово настроенный retrofit client.

Желательно но не обязательно:
 1)Реализовать подходы CLEAN архитектуры в процессе написания тестового задания
 2)Разработать обработку сетевых ошибок с выводом дружественного пользователю сообщения на экран
*/

@AndroidEntryPoint
class MainActivity : BaseActivity(R.layout.activity_main) {

    @ActivityScoped
    @Inject
    lateinit var mainViewModelFactory: MainViewModelFactory

    private val mainViewModel: MainViewModel by viewModels(
        factoryProducer = { mainViewModelFactory }
    )

    override fun onResumeFragments() {
        super.onResumeFragments()
        mainViewModel.goToPopulationStatistics()
    }
}
