package com.example.usapopulation.feature.main.presentation.viewmodel

import com.example.usapopulation.core.presentation.BaseViewModel
import com.example.usapopulation.feature.main.presentation.router.MainRouter

class MainViewModel(
    private val mainRouter: MainRouter
) : BaseViewModel() {

    fun goToPopulationStatistics() {
        mainRouter.goToPopulationStatistics()
    }
}
