package com.example.usapopulation.feature.statistics.presentation.viewmodel

import androidx.compose.runtime.mutableStateOf
import com.example.usapopulation.core.presentation.BaseViewModel
import com.example.usapopulation.feature.statistics.domain.interactor.PopulationStatisticsInteractor
import com.example.usapopulation.feature.statistics.domain.model.PopulationStatisticsEntity
import com.example.usapopulation.feature.statistics.presentation.mapper.PopulationStatisticsErrorMapper

class PopulationStatisticsViewModel(
    private val populationStatisticsInteractor: PopulationStatisticsInteractor,
    private val populationStatisticsErrorMapper: PopulationStatisticsErrorMapper
) : BaseViewModel() {

    val msPopulationStatistics = mutableStateOf<PopulationStatisticsEntity?>(null)

    fun init() {
        loadPopulationStatistics()
    }

    fun retry() {
        loadPopulationStatistics()
    }

    private fun loadPopulationStatistics() {
        jobIO {
            populationStatisticsInteractor
                .populationStatistics()
                .onSuccess {
                    msPopulationStatistics.value = it.sortedByYear()
                }
                .onFailure {
                    msError.value = populationStatisticsErrorMapper.map(it)
                }
        }
    }

    fun toggleYearOrder() {
        msPopulationStatistics.value?.yearOrderDescending =
            msPopulationStatistics.value?.yearOrderDescending != true

        msPopulationStatistics.value = msPopulationStatistics.value?.sortedByYear()
    }

    private fun PopulationStatisticsEntity.sortedByYear() =
        copy(
            data = if (this.yearOrderDescending) {
                data.sortedByDescending { it.year }
            } else {
                data.sortedBy { it.year }
            }
        )
}
