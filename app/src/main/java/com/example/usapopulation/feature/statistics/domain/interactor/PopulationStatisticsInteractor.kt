package com.example.usapopulation.feature.statistics.domain.interactor

import com.example.usapopulation.core.domain.model.DomainError
import com.example.usapopulation.feature.statistics.domain.model.PopulationStatisticsEntity
import com.example.usapopulation.feature.statistics.domain.repository.IPopulationStatisticsRepository

class PopulationStatisticsInteractor(
    private val populationStatisticsRepository: IPopulationStatisticsRepository
) {

    suspend fun populationStatistics(): Result<PopulationStatisticsEntity> =
        populationStatisticsRepository.populationStatistics().takeIf {
            it.getOrNull()?.data.isNullOrEmpty().not()
        } ?: Result.failure(DomainError.EmptyDataError)
}
