package com.example.usapopulation.feature.statistics.data.mapper

import com.example.usapopulation.core.domain.mapper.IMapper
import com.example.usapopulation.feature.statistics.data.model.PopulationStatisticsResponse
import com.example.usapopulation.feature.statistics.domain.model.PopulationStatisticEntity
import com.example.usapopulation.feature.statistics.domain.model.PopulationStatisticsEntity

class PopulationStatisticsMapper : IMapper<PopulationStatisticsResponse, PopulationStatisticsEntity> {

    override fun map(from: PopulationStatisticsResponse): PopulationStatisticsEntity =
        PopulationStatisticsEntity(
            data = from.data?.map {
                PopulationStatisticEntity(
                    nation = it.nation.orEmpty(),
                    year = it.year.orEmpty(),
                    population = it.population.toString()
                )
            }.orEmpty(),
            sourceDescription = from.source
                ?.firstOrNull()
                ?.annotations
                ?.sourceDescription
                .orEmpty()
        )
}
