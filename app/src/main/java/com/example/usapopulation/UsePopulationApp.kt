package com.example.usapopulation

import android.app.Application
import dagger.hilt.android.HiltAndroidApp

@HiltAndroidApp
class UsePopulationApp : Application()
